all: de-burger

clean:
	-rm *.out

purge:
	-rm de-burger

de-burger: de-burger.c jogo.c jogo.h fila.c fila.h pilha.c pilha.h
	gcc de-burger.c jogo.c pilha.c fila.c -Wall -lncurses -g -o de-burger.out
	./de-burger.out