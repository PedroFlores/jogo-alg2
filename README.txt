JOGO
    -Monte corretamente os pedidos conforme eles chegam, se entregar todos pedidos vencerá;
    -Se entregar errado, perderá;
    -OBS: quanto menos tempo demorar pra entregar seu pedido, mais pontos irá marcar;

IMPLEMENTAÇÕES EXTRAS:
    -Movimentação nas setas;
    -Chegada aleatória e automática de pedidos;
    -Tempo para cada entrega do pedido;
    -Cores;
    -Guia pra montagem dos pedidos, visualiar a fila de pedidos, visualiar a pilha de ingredientes;
