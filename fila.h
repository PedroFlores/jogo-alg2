/*estruturas necessárias para a fila */

typedef struct nohF {       
    int pedido;
    struct nohF *prox;    
} nohF_t;

typedef struct fila {
    nohF_t *inicio;
} fila_t;

/*funções da fila*/

void inicializaFila(fila_t *fila);
int fila_vazia(fila_t *fila);
nohF_t * cria_nohF();
void insereFila(fila_t *fila, int x);
int remove_fila(fila_t *fila, WINDOW *main_win);
void exibe_fila(fila_t *fila, int l, int pedido_at, WINDOW *main_win);






